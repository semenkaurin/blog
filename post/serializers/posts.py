from rest_framework import serializers

from ..models import PostsModel


class PostsBaseSerializer(serializers.ModelSerializer):
    title = serializers.CharField(max_length=255)
    content = serializers.CharField()

    class Meta:
        model = PostsModel
        fields = ['title', 'content', 'user_id']


class PostsCreateSerializer(PostsBaseSerializer):

    def create(self, validated_data):
        return PostsModel.objects.create(**validated_data)


class PostsReadSerializer(PostsBaseSerializer):
    user_id = serializers.UUIDField()


class PostsUpdateSerializer(PostsBaseSerializer):

    def update(self, instance, validated_data):
        instance.title = validated_data.get("title", instance.title)
        instance.content = validated_data.get("content", instance.content)
        instance.save()

        return instance
