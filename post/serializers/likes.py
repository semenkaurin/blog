from rest_framework import serializers

from ..models import LikesModel


class LikesBaseSerializer(serializers.ModelSerializer):
    post_id = serializers.UUIDField()

    class Meta:
        model = LikesModel
        fields = ['user_id', 'date', 'post_id']


class LikesCreateSerializer(LikesBaseSerializer):
    pass


class LikesDeleteSerializer(LikesBaseSerializer):
    pass
