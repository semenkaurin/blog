import uuid

from django.db import models

from authentication.models import User
from .posts import PostsModel

class LikesModel(models.Model):
    '''Модель данных представляющая таблицу с лайками для постов'''
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(PostsModel, on_delete=models.CASCADE, null=True)

    class Meta:
        unique_together = [['user', 'post']]
