import uuid

from django.db import models

from authentication.models import User
from group.models import GroupsModel


class PostsModel(models.Model):
    '''Модель данных, описывающая сущность поста в приложении'''
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    title = models.CharField(max_length=255)
    content = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(GroupsModel, on_delete=models.CASCADE, null=True)
