from uuid import UUID

from django.http import HttpRequest
from django.forms.models import model_to_dict
from rest_framework import status
from rest_framework.response import Response

from ..models import PostsModel
from ..serializers.posts import PostsCreateSerializer, PostsReadSerializer, PostsUpdateSerializer


class PostsService:
    '''Сервис для бизнес-логики связанной с постами'''
    def __init__(self, request: HttpRequest):
        self.request = request

    def create(self) -> Response:
        '''Создать новый пост'''
        serializer = PostsCreateSerializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)

        PostsModel.objects.create(**{**serializer.data, 'user_id': self.request.user.id})

        return Response(serializer.data)

    def get_by_id(self, post_id: UUID) -> Response:
        '''Получить пост по его id'''
        if post := PostsModel.objects.get(id=post_id):
            serializer = PostsReadSerializer(data=post)
            serializer.is_valid()

            return Response(model_to_dict(post))

        return Response({'error': 'Object does not exist'})

    def update_by_id(self, post_id: UUID) -> Response:
        '''Получить пост и изменить его'''
        try:
            post = PostsModel.objects.get(id=post_id)
            if post.user_id == self.request.user.id:
                serializer = PostsUpdateSerializer(data=self.request.data, instance=post)
                serializer.is_valid(raise_exception=True)
                serializer.save()

                return Response(serializer.data)

            return Response({'error': 'Object does not exist'})
        except Exception:
            return Response({'error': 'Object does not exist'})

    def delete_by_id(self, post_id: UUID) -> Response:
        '''Удалить пост по его id'''
        try:
            post = PostsModel.objects.get(id=post_id)
            if post.user_id == self.request.user.id:
                post.delete()
                return Response(status=status.HTTP_204_NO_CONTENT)

            return Response({'Error': 'Object does not exist'})
        except Exception:
            return Response({'Error': 'Object does not exist'})
