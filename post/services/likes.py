from uuid import UUID

from django.http import HttpRequest
from django.forms.models import model_to_dict
from rest_framework import status
from rest_framework.response import Response

from ..models import LikesModel
from ..serializers.likes import *


class LikesService:
    '''Сервис для бизнес-логики связанной с лайками'''

    def __init__(self, request):
        self.request = request

    def create(self):
        '''Метод для создания лайка'''
        serializer = LikesCreateSerializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)

        LikesModel.objects.create(**{**serializer.data, 'user_id': self.request.user.id})

        return Response({"post": serializer.data, "user_id": self.request.user.id})

    def get_by_post_id(self, post_id):
        '''Метод для получения лайков по id поста'''
        return Response(LikesModel.objects.filter(post_id=post_id).values())

    def get_by_user_id(self, user_id):
        '''Метод для получения лайков по id пользователя'''
        return Response(LikesModel.objects.filter(user_id=user_id).values())
