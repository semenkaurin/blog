from django.urls import path

from .views import (
    create_post,
    method_controller,
    create_like,
    get_likes_by_post_id,
    get_likes_by_user_id,
)


urlpatterns = [
    path('/', create_post),
    path('/<str:post_id>', method_controller),
    path('/likes/', create_like),
    path('/posts/<str:post_id>', get_likes_by_post_id),
    path('/likes/<str:user_id>', get_likes_by_user_id),
]
