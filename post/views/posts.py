from uuid import UUID

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly

from ..services.posts import PostsService


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_post(request):
    '''Эндпоинт для создания поста'''
    return PostsService(request).create()


@api_view(['GET', 'PUT', 'DELETE'])
def method_controller(request, post_id: UUID):
    if request.method == 'GET':
        return read_post(request, post_id)
    elif request.method == 'PUT':
        return update_post(request, post_id)
    elif request.method == 'DELETE':
        return delete_post(request, post_id)


@permission_classes([IsAuthenticatedOrReadOnly])
def read_post(request, post_id: UUID):
    '''Эндпоинт для получения поста по его id'''
    return PostsService(request).get_by_id(post_id)


@permission_classes([IsAuthenticatedOrReadOnly])
def update_post(request, post_id: UUID):
    '''Эндпоинт для обновления поста по его id'''
    return PostsService(request).update_by_id(post_id)


@permission_classes([IsAuthenticatedOrReadOnly])
def delete_post(request, post_id: UUID):
    '''Эндпоинт для удаления поста по его id'''
    return PostsService(request).delete_by_id(post_id)
