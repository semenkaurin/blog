from uuid import UUID

from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny

from ..models import LikesModel
from ..serializers.likes import LikesCreateSerializer


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_like(request):
    '''Эндпоинт для создания лайка'''
    serializer = LikesCreateSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)

    LikesModel.objects.create(**{**serializer.data, 'user_id': request.user.id})

    return Response({"post": serializer.data, "user_id": request.user.id})


@api_view(['GET'])
@permission_classes([AllowAny])
def get_likes_by_post_id(request, post_id: UUID):
    '''Эндпоинт для получения лайка по id поста'''
    likes = LikesModel.objects.filter(post_id=post_id)

    return Response(likes.values())


@api_view(['GET'])
@permission_classes([AllowAny])
def get_likes_by_user_id(request, user_id):
    '''Эндпоинт для получения лайка по id пользователя'''
    likes = LikesModel.objects.filter(user_id=user_id)

    return Response(likes.values())
