import uuid

from django.db import models

from authentication.models import User


class GroupsModel(models.Model):
    '''Модель данных представляющая таблицу Групп'''
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    name = models.CharField(max_length=255, null=False, unique=True)
    description = models.TextField(null=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
