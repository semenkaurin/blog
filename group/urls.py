from django.urls import path

from .views import create_group, group_methods_controller


urlpatterns = [
    path('/', create_group),
    path('/<str:group_id>', group_methods_controller),
]
