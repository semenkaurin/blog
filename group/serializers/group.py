from rest_framework import serializers

from ..models import GroupsModel


class GroupBaseSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=255)
    description = serializers.CharField(max_length=510, required=False, default=None)

    class Meta:
        model = GroupsModel
        fields = ['owner_id', 'name', 'description', 'id']


class GroupCreateSerializer(GroupBaseSerializer):
    owner = serializers.UUIDField()


class GroupReadSerializer(GroupBaseSerializer):
    owner = serializers.UUIDField(required=True)


class GroupUpdateSerializer(GroupBaseSerializer):
    name = serializers.CharField(max_length=255, required=True)

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.description = validated_data.get("description", instance.description)
        instance.save()

        return instance
