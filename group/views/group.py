from uuid import UUID

from django.http import HttpRequest
from django.forms.models import model_to_dict
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly

from ..models import GroupsModel
from ..serializers.group import GroupCreateSerializer, GroupUpdateSerializer


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_group(request: HttpRequest) -> Response:
    serializer = GroupCreateSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)

    group = GroupsModel.objects.create(**{**serializer.data, 'owner_id': request.user.id})

    return Response(model_to_dict(group))


@api_view(['GET', 'PUT', 'DELETE'])
def group_methods_controller(request: HttpRequest, group_id: UUID) -> Response | dict:
    if request.method == 'GET':
        return get_group(request, group_id)
    if request.method == 'PUT':
        return update_group(request, group_id)
    if request.method == 'DELETE':
        return delete_group(request, group_id)


@permission_classes([IsAuthenticatedOrReadOnly])
def get_group(request: HttpRequest, group_id: UUID) -> Response | dict:
    if group := GroupsModel.objects.get(id=group_id):
        return Response(model_to_dict(group))

    return {'error': 'group does not exist'}


@permission_classes([IsAuthenticated])
def update_group(request: HttpRequest, group_id: UUID) -> Response | dict:
    try:
        group = GroupsModel.objects.get(id=group_id)
        if group.owner_id == request.user.id:
            serializer = GroupUpdateSerializer(data=request.data, instance=group)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            return Response(serializer.data)

        return Response({'error': 'Object does not exist'})
    except Exception:
        return Response({'Error': 'object does not exist'})


@permission_classes([IsAuthenticated])
def delete_group(request: HttpRequest, group_id: UUID) -> Response | dict:
    try:
        group = GroupsModel.objects.get(id=group_id)
        if group.owner_id == request.user.id:
            group.delete()

            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response({'error': 'Object does not exist'})
    except Exception:
        return Response({'Error': 'object does not exist'})
