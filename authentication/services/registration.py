from django.http import HttpRequest
from rest_framework.response import Response

from ..serializer.registration import UserSerializer


class RegistrationService:
    '''Сервис для бизнес-логики, связанной с регистрацией пользователя'''
    def __init__(self, request: HttpRequest):
        self.request = request

    def registration(self) -> Response:
        '''Зарегистрировать пользователя'''
        user_data = UserSerializer(data=self.request.data)
        user_data.is_valid(raise_exception=True)
        user_data.save()

        return Response({'user': user_data.data})
