from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny

from ..services.registration import RegistrationService


@api_view(['POST'])
@permission_classes([AllowAny])
def registrate_user(request):
    '''Эндпоинт для регистрации пользователя'''
    return RegistrationService(request).registration()
