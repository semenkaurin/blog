# Blog



## Стек технологий
* Python 3.10
* Postgres
* DRF
* Django ORM

## Установка
#### 1. Перейти в папку проекта
Это папка с файлом manage.py
Все дальшейшие действия совершаются внутри неё
#### 2. Создать и активировать виртуальное окружение
```
python -m venv venv
source venv/bin/activate - Linux
venv/Scripts/activate - Windows
```
venv - путь к папке виртуального окружения
можно оставить просто venv, и тогда папка создастся в текущей
#### 3. Установить зависимости
```
pip install -r requirements.txt
```
#### 4. Добавить переменные окружения
```
Обозначения:                             Пример:
DB__NAME: имя БД                         DB_NAME=blog
DB__USER: имя юзера в БД                 DB_USER=postgres
DB_USER_PASSWORD: пароль юзера в БД      DB_USER_PASSWORD=postgres
DB__HOST: хост БД                        DB_HOST=localhost
DB__PORT: порт БД                        DB_PORT=5432
```
#### 5. Стартовать WSGI Server
```
python manage.py runserver <port> | <host:port>
```